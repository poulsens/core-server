import express from "express"
import { graphqlHTTP } from "express-graphql"
import { schema } from "./src/schema/schema"
import { connect, connection } from "mongoose"
import cors from "cors"

const app = express()

// export MONGODB_URL="mongodb://core:corepw@localhost:27017"


const dbConnectionUrl = process.env.MONGODB_URL || 
'mongodb://127.0.0.1:27017/CORE';

connect(dbConnectionUrl)
connection
    .once('open', () => console.log("Connected to MongoDB instance"))
    .on('error', error => console.log('Error connecting to MongoLab:', error));

app.use(cors())



const loggingMiddleware = (req: any, res: any, next: any) => {

  console.log('ip:', req.ip);
  var today = new Date();

var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

var dateTime = date+' '+time;
   console.log('at: ',dateTime);
  next();

}



var root = {

  ip: function (args: any, request: any) {

    return request.ip;

  }

};


app.use(loggingMiddleware);
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))



app.listen(8080, () => {
  console.log('Listening');
  console.log('Ready');
})

