import { PlantModel, PlantTC } from "../../models/plant";
import { UnitTC } from "../../models/unit";

export const addComputedVariableToUnit = {
    type: PlantTC, 
    args: {plantId: 'String', unitName : 'String', computedVariable: 'UnitComputedVariablesInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.computedVariables": args.computedVariable}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const editComputedVariableInUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', computedVariable: 'UnitComputedVariablesInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].computedVariables.$[j]": {
                    name: args.computedVariable.name,
                    code: args.computedVariable.code,
                    description: args.computedVariable.description,
                    type: args.computedVariable.type,
                    unit: args.computedVariable.unit,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.computedVariable._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteComputedVariableFromUnit = {
    type: UnitTC,
    args: {plantId: 'String', unitName : 'String', computedVariableId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.computedVariables": {"_id" : args.computedVariableId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

