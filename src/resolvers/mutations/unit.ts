import { Plant, PlantModel, PlantTC } from "../../models/plant"
import { Unit } from "../../models/unit"

export const AddUnitToPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', unit: 'UnitInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId}, {$push : {units: args.unit}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const UpdateUnitInPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', unit: 'UnitInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const { plantId , unit } = args; 
        console.log("Updating args: ", args)
        const { _id, shortName, displayName, shortDescription, description, operationalStates, transitionConditions } = unit;
        const plant = await PlantModel.updateOne({_id: plantId, "units._id": _id}, {$set : {"units.$.shortName": shortName, "units.$.displayName": displayName, "units.$.shortDescription": shortDescription, "units.$.description": description, "units.$.operationalStates": operationalStates, "units.$.transitionConditions": transitionConditions }})
        console.log(plant)
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const RemoveUnitInPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', unitId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        let queue = [] 
        queue.push(args.unitId)
        let curr = {}
        let size,i; 
        while(queue.length > 0){
            size = queue.length
            for(i=size; i; i--){
                curr = queue.shift().toString()
                const plantUnits = await PlantModel.updateOne({_id: args.plantId}, {$pull : {"units": {"_id" : curr}}})
                const plantActuators = await PlantModel.updateOne({_id: args.plantId}, {$pull : {"actuators": {"parent" : curr}}})
                const plant : Plant | null= await PlantModel.findOne({_id: args.plantId})
                if(plant && plant.units){
                    const childUnits = plant.units.filter((unit : Unit) => unit.parent === curr)
                    childUnits.forEach((child:any) => queue.push(child._id))
                }
            }
        }
        return PlantModel.findOne({_id: args.plantId})
    }
}


export const AddObjectBehaviourToUnit = { 
    type: PlantTC,
    args: {plantId: 'String', unitId: 'String', objectBehaviour: 'UnitObjectBehaviourInput'},
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.objectBehaviour": args.objectBehaviour}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const EditObjectBehaviourInUnit = { 
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', objectBehaviour: 'UnitObjectBehaviourInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].computedVariables.$[j]": {
                    state: args.objectBehaviour.state,
                    behaviour: args.objectBehaviour.behaviour,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.objectBehaviour._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const DeleteObjectBehaviourInUnit =  {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', objectBehaviourId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.objectBehaviour": {"_id" : args.objectBehaviourId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}