import { PlantModel, PlantTC } from "../../models/plant";
import { UnitTC } from "../../models/unit";

export const addParamterToUnit = {
    type: UnitTC, 
    args: {plantId: 'String', unitName : 'String', parameter: 'UnitParametersInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.parameters": args.parameter}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId}).populate('units').findOne({shortName: args.unitName})       
    }
}

export const editParameterInUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', parameter: 'UnitParametersInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].parameters.$[j]": {
                    name: args.parameter.name,
                    description: args.parameter.description,
                    defaultValue: args.parameter.defaultValue,
                    unit: args.parameter.unit,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.parameter._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteParameterFromUnit = {
    type: UnitTC,
    args: {plantId: 'String', unitName : 'String', parameterId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.parameters": {"_id" : args.parameterId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

