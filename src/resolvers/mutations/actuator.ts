import { PlantModel, PlantTC } from "../../models/plant"

export const AddActuatorToPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', actuator: 'ActuatorInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId}, {$push : {"actuators": args.actuator}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const UpdateActuatorInPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', actuator: 'ActuatorInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "actuators._id": args.actuator._id}, {$set : {"actuators.$": args.actuator}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const RemoveActuatorInPlant = { 
    type: PlantTC, 
    args: {plantId: 'String', actuatorId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId}, {$pull : {"actuators": {"_id" : args.actuatorId}}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const AddObjectBehaviourToActuator = { 
    type: PlantTC,
    args: {plantId: 'String', actuatorId: 'String', objectBehaviour: 'ActuatorObjectBehaviourInput'},
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "actuators.shortName": args.actuatorName}, {$push : {"actuators.$.objectBehaviour": args.objectBehaviour}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const EditObjectBehaviourInActuator = { 
    type: PlantTC,
    args: {plantId: 'String', actuatorName : 'String', objectBehaviour: 'ActuatorObjectBehaviourInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "actuators.$[i].computedVariables.$[j]": {
                    state: args.objectBehaviour.state,
                    behaviour: args.objectBehaviour.behaviour,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.actuatorName},
                {"j._id": args.objectBehaviour._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}


export const DeleteObjectBehaviourInActuator =  {
    type: PlantTC,
    args: {plantId: 'String', actuatorName : 'String', objectBehaviourId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "actuators.shortName" : args.actuatorName}, {$pull : {"actuators.$.objectBehaviour": {"_id" : args.objectBehaviourId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}