import { Plant, PlantModel, PlantTC } from "../../models/plant"
import { Unit } from "../../models/unit"

export const AddOperationalStateToUnit = { 
    type: PlantTC, 
    args: {plantId: 'String!', unitName: 'String!', operationalState: 'UnitOperationalStatesInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.operationalStates": args.operationalState}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const EditOperationalStateInUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', operationalState: 'UnitOperationalStatesInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].operationalStates.$[j]": {
                    step: args.operationalState.step,
                    name: args.operationalState.name,
                    description: args.operationalState.description,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.operationalState._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const DeleteOperationalStateFromUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', operationalStateId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.operationalStates": {"_id" : args.operationalStateId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const AddTransitionConditionToUnit = { 
    type: PlantTC, 
    args: {plantId: 'String!', unitName: 'String!', transitionConditions: 'UnitTransitionConditionsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.tranisitionConditions": args.transitionConditions}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const EditTransitionConditionInUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', transitionCondition: 'UnitTransitionConditionsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].transitionConditions.$[j]": {
                    name: args.transitionCondition.name,
                    start: args.transitionCondition.start,
                    stop: args.transitionCondition.stop,
                    condition: args.transitionCondition.condition,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.transitionCondition._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const DeleteTransitionConditionFromUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', transitionConditionId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.transitionConditions": {"_id" : args.transitionConditionId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}
