import { PlantModel, PlantTC } from "../../models/plant";
import { UnitTC } from "../../models/unit";

export const addUserCommandToUnit = {
    type: UnitTC, 
    args: {plantId: 'String', unitName : 'String', userCommand: 'UnitUserCommandsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        console.log(args.unitName)
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.userCommands": args.userCommand}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId}).populate('units').findOne({shortName: args.unitName})       
    }
}

export const editUserCommandInUnit = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', userCommand: 'UnitUserCommandsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].userCommands.$[j]": {
                    command: args.userCommand.command,
                    type: args.userCommand.type,
                    description: args.userCommand.description,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.userCommand._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteUserCommandFromUnit = {
    type: UnitTC,
    args: {plantId: 'String', unitName : 'String', userCommandId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.userCommands": {"_id" : args.userCommandId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

