import { PlantModel, PlantTC } from "../../models/plant";
import { UnitTC } from "../../models/unit";

export const addUnitAlarm = {
    type: UnitTC, 
    args: {plantId: 'String', unitName : 'String', alarm: 'UnitUnitAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        console.log(args.unitName)
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.unitAlarms": args.alarm}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId}).populate('units').findOne({shortName: args.unitName})       
    }
}

export const editUnitAlarm = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', alarm: 'UnitUnitAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].unitAlarms.$[j]": {
                    name: args.alarm.name,
                    condition: args.alarm.condition,
                    temporisation: args.alarm.temporisation,
                    message: args.alarm.message,
                    action: args.alarm.action,
                    group: args.alarm.group,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.alarm._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteUnitAlarm = {
    type: UnitTC,
    args: {plantId: 'String', unitName : 'String', alarmId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.unitAlarms": {"_id" : args.alarmId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const addCCCAlarm = {
    type: UnitTC, 
    args: {plantId: 'String', unitName : 'String', alarm: 'UnitCccAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName": args.unitName}, {$push : {"units.$.cccAlarms": args.alarm}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId}).populate('units').findOne({shortName: args.unitName})       
    }
}

export const editCCCAlarm = {
    type: PlantTC,
    args: {plantId: 'String', unitName : 'String', alarm: 'UnitCccAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "units.$[i].cccAlarms.$[j]": {
                    name: args.alarm.name,
                    grouping: args.alarm.grouping,
                    message: args.alarm.message,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.unitName},
                {"j._id": args.alarm._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteCCCAlarm = {
    type: UnitTC,
    args: {plantId: 'String', unitName : 'String', alarmId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "units.shortName" : args.unitName}, {$pull : {"units.$.cccAlarms": {"_id" : args.alarmId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const addActuatorAlarm = {
    type: PlantTC, 
    args: {plantId: 'String', actuatorName : 'String', alarm: 'ActuatorAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateOne({_id: args.plantId, "actuators.shortName": args.actuatorName}, {$push : {"actuators.$.alarms": args.alarm}})
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})
    }
}

export const editActuatorAlarm = {
    type: PlantTC,
    args: {plantId: 'String', actuatorName : 'String', alarm: 'ActuatorAlarmsInput'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => {
        const plant = await PlantModel.updateMany({_id: args.plantId}, {
            $set: {
                "actuators.$[i].alarms.$[j]": {
                    name: args.alarm.name,
                    condition: args.alarm.condition,
                    temporisation: args.alarm.temporisation,
                    message: args.alarm.message,
                    action: args.alarm.action,
                    cccGroup: args.alarm.cccGroup,
                }
            }
        }, {
            arrayFilters: [
                {"i.shortName": args.actuatorName},
                {"j._id": args.alarm._id},
            ]
        })
        if (!plant) return null 
        return PlantModel.findOne({_id: args.plantId})    
    }
}

export const deleteActuatorAlarm = {
    type: UnitTC,
    args: {plantId: 'String', actuatorName : 'String', alarmId: 'String'}, 
    //@ts-ignore
    resolve: async (source, args, context, info) => { 
        const plant = await PlantModel.updateOne({_id: args.plantId, "actuators.shortName" : args.actuatorName}, {$pull : {"actuators.$.alarms": {"_id" : args.alarmId }}})
        if (!plant) return null
        return PlantModel.findOne({_id: args.plantId})    
    }
}