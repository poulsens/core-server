import { schemaComposer } from "graphql-compose"
import { PlantModel } from "../../models/plant"
import { UnitModel, UnitTC } from "../../models/unit"

export const getUnitFromPlant = schemaComposer.createResolver({ 
    name: 'getUnitFromPlant',
    type: UnitTC, 
    args: {plantId: 'String!', unitName: 'String!'}, 
    //@ts-ignore
    resolve: async ({source, args, context, info}) => {
        const res = await PlantModel.findOne({_id: args.plantId}, 'units').exec() 
        if( res && res.units){
            return res.units?.filter(unit => unit.shortName === args.unitName)[0]
        }
        return null
    }
});

export const getChildUnitsOfUnit = schemaComposer.createResolver({
    name: 'getChildrenOfUnit',
    type: [UnitTC], 
    args: {plantId: 'String!', unitName: 'String!'},
    resolve: async ({source, args, context, info}) => { 
        const res = await PlantModel.findOne({_id: args.plantId}, 'units').exec() 
        if( res && res.units){
            const unit = res.units?.filter(unit => unit.shortName === args.unitName)[0]; 
            //@ts-ignore
            return res.units?.filter(u => u.parent === unit._id.toString());
        }
        return null
    }
});

export const getChildActuatorsOfUnit = schemaComposer.createResolver({
    name: 'getChildrenOfUnit',
    type: [UnitTC], 
    args: {plantId: 'String!', unitName: 'String!'},
    resolve: async ({source, args, context, info}) => { 
        const res = await PlantModel.findOne({_id: args.plantId}).exec() 
        if( res && res.units){
            const unit = res.units?.filter(unit => unit.shortName === args.unitName)[0]; 
            //@ts-ignore
             return res.actuators?.filter(a => a.parent === unit._id.toString()); 
        }
        return null
    }
});
