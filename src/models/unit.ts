import { Schema, model } from "mongoose";
import { composeMongoose } from 'graphql-compose-mongoose'; 
import { schemaComposer } from "graphql-compose";

export interface Unit { 
    shortName: string, 
    displayName: string, 
    shortDescription: string, 
    description: string, 
    parent: string | null, 
    operationalStates: OperationalState[], 
    transitionConditions: TransitionCondition[],
    objectBehaviour: ObjectBehaviour[],
    unitAlarms: UnitAlarm[],
    cccAlarms: CCCAlarm[],
    userCommands: UserCommand[],
    parameters: Parameter[],
    computedVariables: ComputedVariable[],
}

interface ObjectBehaviour { 
    state: string,
    behaviour: string,
}

interface OperationalState { 
    step: string, 
    name: string,
    description: string 
}

interface TransitionCondition { 
    start: string, 
    stop: string, 
    name: string, 
    condition: string 
}

interface UnitAlarm { 
    name: string, 
    condition: string,
    temporisation: string, 
    message: string,
    action: string, 
    group: string,
}

interface CCCAlarm { 
    name: string,
    grouping: string[], 
    message: string,
}

interface UserCommand{ 
    command: string,
    type: string, 
    description: string,
}

interface Parameter {
    name: string,
    description: string,
    defaultValue: number, 
    unit: string,
}

interface ComputedVariable { 
    name: string, 
    code: string, 
    description: string,
    type: string,
    unit: string,
}

const OperationalStateSchema = new Schema<OperationalState>({ 
    step: String, 
    name: String,
    description: String 
})

const TransitionConditionSchema = new Schema<TransitionCondition>({ 
    start: String, 
    stop: String, 
    name: String, 
    condition: String 
})

const ObjectBehaviourSchema = new Schema<ObjectBehaviour>({
    state: String,
    behaviour: String,
})

const UnitAlarmSchema = new Schema<UnitAlarm>({
    name: String,
    condition: String,
    temporisation: String,
    message: String,
    action: String,
    group: String,
})

const CCCAlarmSchema = new Schema<CCCAlarm>({
    name: String,
    grouping: [String],
    message: String,
})

const UserCommandSchema = new Schema<UserCommand>({
    command: String,
    type: String,
    description: String,
})

const ParameterSchema = new Schema<Parameter>({
    name: String,
    description: String,
    defaultValue: Number,
    unit: String,
})

const ComputedVariableSchema = new Schema<ComputedVariable>({
    name: String,
    code: String,
    description: String,
    type: String,
    unit: String,
})

export const UnitSchema = new Schema<Unit>({ 
    shortName: String, 
    displayName: String, 
    shortDescription: String, 
    description: String, 
    parent: String, 
    operationalStates: [OperationalStateSchema], 
    transitionConditions: [TransitionConditionSchema], 
    objectBehaviour: [ObjectBehaviourSchema],
    unitAlarms: [UnitAlarmSchema],
    cccAlarms: [CCCAlarmSchema],
    userCommands: [UserCommandSchema],
    parameters: [ParameterSchema],
    computedVariables: [ComputedVariableSchema],    
})

const customoizationOptions = {}

export const UnitModel = model('Unit', UnitSchema)
const OperationalStateModel = model('OperationalState', OperationalStateSchema)
const TransitionConditionModel = model('TransitionCondition', TransitionConditionSchema)

//@ts-ignore
export const UnitTC = composeMongoose(UnitModel, customoizationOptions)
//@ts-ignore
export const OperationalStateTC = composeMongoose(OperationalStateModel, customoizationOptions)
//@ts-ignore 
export const TransitionConditionTC = composeMongoose(TransitionConditionModel, customoizationOptions)

export const UnitInput = schemaComposer.createObjectTC(`
    type UserInput {
        _id: String
        shortName: String, 
        displayName: String, 
        shortDescription: String, 
        description: String, 
        parent: String,
        operationStates: [OperationalStateTC]
        transitionConditions : [TransitionConditionTC]
    }
`)

export const ActuatorInput = schemaComposer.createObjectTC(`
    type ActuatorInput {
        _id: String
        shortName: String, 
        shortDescription: String, 
        description: String, 
        parent: String,
        failSafe: String, 
        command: [CommandTC], 
        feedback: [FeedbackTC]
    }
`)

