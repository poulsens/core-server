import { Schema, model } from "mongoose";
import { composeMongoose } from 'graphql-compose-mongoose'; 
import { schemaComposer } from "graphql-compose";

export interface _Command {
    name: string;
    description: string;
  }
  
export interface AnalogCommand extends _Command {
    rangeMin: number;
    rangeMax: number;
    unit: string;
}

export interface DigitalCommand extends _Command {
    status: string;
    labelOn: string;
    labelOff: string;
}

export type Command = AnalogCommand | DigitalCommand;

export interface _Feedback {
    name: string;
    description: string;
}

export interface DigitalFeedback extends _Feedback {
    status: string;
}

export interface AnalogFeedback extends _Feedback {
    rangeMin: number;
    rangeMax: number;
    unit: string;
}

export type Feedback = DigitalFeedback | AnalogFeedback;

interface ActuatorAlarm { 
    name: string;
    condition: string;
    temporisation: string; 
    message: string;
    action: string;
    cccGroup: string;
}

interface ObjectBehaviour { 
    state: string,
    behaviour: string,
}

export interface Actuator { 
    shortName: string, 
    shortDescription: string, 
    description: string, 
    parent: string | null,
    failSafe: string, 
    command: Command[], 
    feedback: Feedback[], 
    alarms: ActuatorAlarm[],
    objectBehaviour: ObjectBehaviour[],
}

export const CommandSchema = new Schema<Command>({ 
    name: String!,
    description: String!,
    rangeMin: Number,
    rangeMax: Number,
    unit: String,
    status: String,
    labelOn: String,
    labelOff: String
})

export const FeedbackSchema = new Schema<Feedback>({ 
    name: String!,
    description: String!,
    rangeMin: Number,
    rangeMax: Number,
    unit: String,
    status: String,
})

export const ObjectBehaviour = new Schema<ObjectBehaviour>({
    state: String,
    behaviour: String,
})

const ActuatorAlarmSchema = new Schema<ActuatorAlarm>({
    name: String!,
    condition: String!,
    temporisation: String,
    message: String,
    action: String!,
    cccGroup: String,
})

export const ActuatorSchema = new Schema<Actuator>({
    shortName: String!, 
    shortDescription: String, 
    description: String, 
    parent: String!, 
    failSafe: String!,
    command: [CommandSchema], 
    feedback: [FeedbackSchema], 
    objectBehaviour: [ObjectBehaviour],
    alarms: [ActuatorAlarmSchema],
})

const ActuatorModel = model('Actuator', ActuatorSchema)
const CommandModel = model('Command', CommandSchema)
const FeedbackModel = model('Feedback', FeedbackSchema)

const customoizationOptions = {}

//@ts-ignore
export const ActuatorTC = composeMongoose(ActuatorModel, customoizationOptions)
//@ts-ignore
export const CommandTC = composeMongoose(CommandModel, customoizationOptions)
//@ts-ignore
export const FeedbackTC = composeMongoose(FeedbackModel, customoizationOptions)

// export const ActuatorInput = schemaComposer.createObjectTC(`
//     type ActuatorInput {
//         _id: String
//         shortName: String, 
//         shortDescription: String, 
//         description: String, 
//         parent: String,
//         failSafe: String, 
//         command: [CommandTC], 
//         feedback: [FeedbackTC]
//     }
// `)