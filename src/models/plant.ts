import { Schema, model } from "mongoose";
import { composeMongoose } from 'graphql-compose-mongoose'; 
import { Unit, UnitSchema } from "./unit";
import { Actuator, ActuatorSchema } from "./actuator";

export interface Plant { 
    name: string, 
    purpose: string, 
    generalCapabilties: string, 
    dysfunctionalCapabilties: string
    firemanCubicle? : AdditionalPlantElement[], 
    hardcodedSignal? : AdditionalPlantElement[], 
    softwareSignal? : AdditionalPlantElement[], 
    units?: Unit[], 
    actuators?: Actuator[]
}

interface AdditionalPlantElement {
    description: string, 
    data : AdditionalPlantElementData[]
}

interface AdditionalPlantElementData {
    signal: string, 
    action: string, 
    type: SignalType, 
    feedback: string
}

enum SignalType {
    wired = "WIRED", 
    coded = "CODED"
}

const FiremanCubicleData = new Schema<AdditionalPlantElementData>({
    signal: String, 
    action: String, 
    type: {
        type: String, 
        enum: SignalType, 
    }, 
    feedback: String
})

const FiremanCubicleSchema = new Schema<AdditionalPlantElement>({
    description: String, 
    data: [FiremanCubicleData]
})
const HardCodedSignalData = new Schema<AdditionalPlantElementData>({
    signal: String, 
    action: String, 
    type: {
        type: String, 
        enum: SignalType, 
    }, 
    feedback: String

})

const HardCodedSignalSchema = new Schema<AdditionalPlantElement>({
    description: String, 
    data: [HardCodedSignalData]
})

const SoftwareSignalData = new Schema<AdditionalPlantElementData>({
    signal: String, 
    action: String, 
    type: {
        type: String, 
        enum: SignalType, 
    }, 
    feedback: String

})

const SoftwareSingalSchema = new Schema<AdditionalPlantElement>({
    description: String, 
    data: [SoftwareSignalData]
})


export const PlantSchema = new Schema<Plant>({
    name: {type: String, required: true},
    purpose: String, 
    generalCapabilties: String,
    dysfunctionalCapabilties: String, 
    firemanCubicle: [FiremanCubicleSchema], 
    hardcodedSignal: [HardCodedSignalSchema], 
    softwareSignal: [SoftwareSingalSchema], 
    units: [UnitSchema], 
    actuators: [ActuatorSchema]
}) 

export const PlantModel = model('Plant', PlantSchema)
export const FiremanCubicleModel = model('FiremanCubicle', FiremanCubicleSchema)
export const HardcodedSignalModel = model('HardcodedSignal', HardCodedSignalSchema)
export const SoftwareSignalModel = model('SoftwareSignal', SoftwareSingalSchema)

const customoizationOptions = {}

 // @ts-ignore
export const PlantTC = composeMongoose(PlantModel, customoizationOptions)
 // @ts-ignore
export const FiremanCubicleTC = composeMongoose(FiremanCubicleModel, customoizationOptions)
 // @ts-ignore
export const HardcodedSignalTC = composeMongoose(HardcodedSignalModel, customoizationOptions)
 // @ts-ignore
export const SoftwareSignalTC = composeMongoose(SoftwareSignalModel, customoizationOptions) 


