import { schemaComposer } from 'graphql-compose'
import { PlantTC } from "../models/plant";
import { AddActuatorToPlant, AddObjectBehaviourToActuator, DeleteObjectBehaviourInActuator, EditObjectBehaviourInActuator, RemoveActuatorInPlant, UpdateActuatorInPlant } from '../resolvers/mutations/actuator';
import { addActuatorAlarm, addCCCAlarm, addUnitAlarm, deleteActuatorAlarm, deleteCCCAlarm, deleteUnitAlarm, editActuatorAlarm, editCCCAlarm, editUnitAlarm } from '../resolvers/mutations/alarm';
import { addComputedVariableToUnit, deleteComputedVariableFromUnit, editComputedVariableInUnit } from '../resolvers/mutations/computedVariables';
import { AddOperationalStateToUnit, AddTransitionConditionToUnit, DeleteOperationalStateFromUnit, DeleteTransitionConditionFromUnit, EditOperationalStateInUnit, EditTransitionConditionInUnit } from '../resolvers/mutations/operationalStates';
import { addParamterToUnit, deleteParameterFromUnit, editParameterInUnit } from '../resolvers/mutations/parameters';
import { AddObjectBehaviourToUnit, AddUnitToPlant, DeleteObjectBehaviourInUnit, EditObjectBehaviourInUnit, RemoveUnitInPlant, UpdateUnitInPlant } from '../resolvers/mutations/unit';
import { addUserCommandToUnit, deleteUserCommandFromUnit, editUserCommandInUnit } from '../resolvers/mutations/userCommands';
import { getChildActuatorsOfUnit, getChildUnitsOfUnit, getUnitFromPlant } from '../resolvers/queries/units';

schemaComposer.Query.addFields({
    plantById: PlantTC.mongooseResolvers.findById(), 
    plantByIds: PlantTC.mongooseResolvers.findByIds(),
    plantFindOne: PlantTC.mongooseResolvers.findOne(),
    plantMany: PlantTC.mongooseResolvers.findMany(), 
    unitByName: getUnitFromPlant, 
    childUnitsOfUnit: getChildUnitsOfUnit,
    childActuatorsOfUnit: getChildActuatorsOfUnit,
});

schemaComposer.Mutation.addFields({
    PlantCreateOne: PlantTC.mongooseResolvers.createOne(),
    PlantCreateMany: PlantTC.mongooseResolvers.createMany(),
    PlantUpdateById: PlantTC.mongooseResolvers.updateById(),
    PlantUpdateOne: PlantTC.mongooseResolvers.updateOne(),
    PlantUpdateMany: PlantTC.mongooseResolvers.updateMany(),
    PlantRemoveById: PlantTC.mongooseResolvers.removeById(),
    PlantRemoveOne: PlantTC.mongooseResolvers.removeOne(),
    PlantRemoveMany: PlantTC.mongooseResolvers.removeMany(),
    AddUnitToPlant: AddUnitToPlant,
    UpdateUnitInPlant: UpdateUnitInPlant, 
    AddActuatorToPlant: AddActuatorToPlant, 
    UpdateActuatorInPlant: UpdateActuatorInPlant, 
    RemoveUnitInPlant: RemoveUnitInPlant, 
    RemoveActuatorInPlant: RemoveActuatorInPlant, 
    AddOperationalStateToUnit: AddOperationalStateToUnit,
    AddTransitionConditionToUnit: AddTransitionConditionToUnit,
    EditOperationalStateInUnit: EditOperationalStateInUnit, 
    EditTransitionConditionInUnit: EditTransitionConditionInUnit,
    DeleteOpetationalStateFromUnit: DeleteOperationalStateFromUnit,
    DeleteTransitionConditionFromUnit: DeleteTransitionConditionFromUnit,
    AddObjectBehaviourInUnit: AddObjectBehaviourToUnit, 
    AddObjectBehaviourToActuator: AddObjectBehaviourToActuator, 
    EditObjectBehaviourToUnit: EditObjectBehaviourInUnit, 
    EditObjectBehaviourToActuator: EditObjectBehaviourInActuator,
    DeleteObjectBehaviourFromUnit: DeleteObjectBehaviourInUnit,
    DeleteObjectBehaviourFromActuator: DeleteObjectBehaviourInActuator,
    AddUserCommandToUnit: addUserCommandToUnit, 
    EditUserCommandInUnit: editUserCommandInUnit, 
    DeleteUserCommandFromUnit: deleteUserCommandFromUnit,
    AddParameterToUnit: addParamterToUnit,
    EditParameterInUnit: editParameterInUnit,
    DeleteParameterFromUnit: deleteParameterFromUnit,
    AddComputedVariableToUnit: addComputedVariableToUnit, 
    EditComputedVariableInUnit: editComputedVariableInUnit,
    DeleteComputedVariableFromUnit: deleteComputedVariableFromUnit,
    AddUnitAlarm : addUnitAlarm, 
    EditUnitAlarm : editUnitAlarm, 
    DeleteUnitAlarm : deleteUnitAlarm,
    AddCCCAlarm : addCCCAlarm,
    EditCCCAlarm : editCCCAlarm,
    DeleteCCCAlarm : deleteCCCAlarm,
    AddActuatorAlarm: addActuatorAlarm, 
    EditActuatorAlarm: editActuatorAlarm, 
    DeleteActuatorAlarm: deleteActuatorAlarm,
})

export const schema = schemaComposer.buildSchema(); 
